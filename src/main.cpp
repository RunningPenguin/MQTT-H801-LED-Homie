#include <Homie.h>

#define FW_NAME "Homie-H801-RGB"
#define FW_VERSION "0.5.2"

/* Magic sequence for Autodetectable Binary Upload */
const char *__FLAGGED_FW_NAME = "\xbf\x84\xe4\x13\x54" FW_NAME "\x93\x44\x6b\xa7\x75";
const char *__FLAGGED_FW_VERSION = "\x6a\x3f\x3e\x0e\xe1" FW_VERSION "\xb0\x30\x48\xd4\x1a";
/* End of magic sequence for Autodetectable Binary Upload */


#include "H801Node.hpp"


/* -------------------------OTA update -------------------*/
// #include <ArduinoOTA.h> not possible too less flash space
/* -------------------------------------------------------*/

#include <Arduino.h>


H801Node h801node("CCTLicht", "H801");



void onHomieEvent(const HomieEvent& event) {
  switch(event.type) {
    case HomieEventType::WIFI_CONNECTED:
      // Do whatever you want when Wi-Fi is connected in normal mode
      h801node.turnLightOn();
      // You can use event.ip, event.gateway, event.mask
    break;
    case HomieEventType::MQTT_READY:
      h801node.turnLightOn();
      h801node.returnStatusToOpenhab();
      // Do whatever you want when MQTT is connected in normal mode
      break;
  }
}


//rough hack to have some light even if WLAN/MQTT is not connected
#define DIMMER_1_PIN_RED 15
#define DIMMER_2_PIN_GREEN 13
#define DIMMER_3_PIN_BLUE 12
#define DIMMER_4_PIN_W1 14
#define DIMMER_5_PIN_W2 4

uint8_t _pins[5] = {DIMMER_1_PIN_RED, DIMMER_2_PIN_GREEN, DIMMER_3_PIN_BLUE, DIMMER_4_PIN_W1, DIMMER_5_PIN_W2};
//rough hack end

void setup()
{
//rough hack to have some light even if WLAN/MQTT is not connected
  pinMode(_pins[0], OUTPUT);
  pinMode(_pins[1], OUTPUT);
  pinMode(_pins[2], OUTPUT);
  pinMode(_pins[3], OUTPUT);
  pinMode(_pins[4], OUTPUT);

  analogWrite(_pins[0], 1023);
  analogWrite(_pins[1], 0);
  analogWrite(_pins[2], 0);
  analogWrite(_pins[3], 614);
  analogWrite(_pins[4], 614);
//rough hack end

/* ----------------MQTT Homie Convention -----------------*/
  Homie_setFirmware(FW_NAME, FW_VERSION);

  // !!!Attention!!!
  // The H801 controller uses serial1 for output
  // Serial1.begin(115200);
  // Homie.setLoggingPrinter(&Serial1);

  // The green LED on H801 is connected to Serial
  Homie.setLedPin(LED_PIN_RED, LOW);

  Homie.disableResetTrigger();

  h801node.beforeSetup();
  Homie.onEvent(onHomieEvent);
  // Homie.setSetupFunction(setupHandler);
  // Homie.setLoopFunction(loopHandler);

  Homie.setup();
/* -------------------------------------------------------*/

  dimmer.init();
  virt_dmx.init();
}

void loop()
{
  virt_dmx.loop();
  dimmer.loop();
  Homie.loop();
}

