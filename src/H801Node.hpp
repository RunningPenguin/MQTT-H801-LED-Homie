/*
 * h801node.hpp
 * Homie node for a h801 five channel LED controller
 * usually RGBWW
 *
 * Version: 1.0
 * Author: Lübbe Onken (http://github.com/luebbe)
 */

#pragma once

#include <Homie.hpp>


#include "hsv2rgb.h"

// #define DEBUG // uncomment this line if you want to see the start and end values in fading
// #define DEBUGFADE // uncomment this line if you want to see all the single steps in fading

#define DIMMER_1_PIN_RED 15
#define DIMMER_2_PIN_GREEN 13
#define DIMMER_3_PIN_BLUE 12
#define DIMMER_4_PIN_W1 14
#define DIMMER_5_PIN_W2 4

#define LED_PIN_GREEN 1
#define LED_PIN_RED 5

// Used when LIGHT_USE_WHITE AND LIGHT_USE_CCT is 1 - (1000000/Kelvin = MiReds)
// Warning! Don't change this yet, NOT FULLY IMPLEMENTED!
#define LIGHT_COLDWHITE_MIRED   153         // Coldwhite Strip, Value must be __BELOW__ W2!! (Default: 6535 Kelvin/153 MiRed)
#define LIGHT_WARMWHITE_MIRED   500         // Warmwhite Strip, Value must be __ABOVE__ W1!! (Default: 2000 Kelvin/500 MiRed)

#include "dimmer.h"


class H801Node : public HomieNode
{
private:
  const char *cCaption = "• H801 RGBWW Controller:";

  // Settable properties
  const char *cPropOn = "on"; // Not really settable, always reports true, because the controller is connected to a physical switch
  const char *cPropEffectMode = "effect";
  const char *cPropRGB = "rgb";
  const char *cPropHSV = "hsv";
  const char *cPropSpeed = "speed";
  const char *cPropRed = "red";
  const char *cPropGreen = "green";
  const char *cPropBlue = "blue";
  const char *cPropWhite1 = "white1";
  const char *cPropWhite2 = "white2";
  const char *cPropColorTemp = "colorTemp";
  const char *cPropCctBrightness = "cctBrightness";
  const char *cPropRgbBrightness = "rgbBrightness";


uint8_t speed = 106;
uint8_t oldSpeed = 106;
bool fadingSpeedHigh = false;
float Kelvin = 4600; //default 50% WW and 50% CW = middle of color temperature

//set default brightness values, but does apply only after MQTT is setup and ready. For default values without MQTT/WLAN please look at dmxqueue.cpp
uint8_t cctBrightness = 80; // 80% WW and CW brightness as default
uint32_t rgb = 100100100; //100100100=100% all three channels 80080080=>80% all three channels //default 0

  enum COLORINDEX // Enum for array indices
  {
    RED = 1,
    GREEN,
    BLUE,
    WHITEWW1,
    WHITECW1
  };





struct CHSV _curHsv; // The current HSV values




  uint8_t _curValue[6] = {40, 60, 20, 10, 100, 20}; // The current percent value of the dimmer. 6 dimensions because we had to start counting from 1 not from 0 because auf the dimmer-lib

  uint32_t _curWhiteValues = 0;
  // uint8_t _endValue[5] = {0, 0, 0, 0, 0};      // The target percent value of the dimmer
  // uint8_t _endValueLast[5] = {0, 0, 0, 0, 0};      // The target percent value of the dimmer
  // uint8_t _setPointValue[5] = {0, 0, 0, 80, 80};      // The target percent value of the dimmer
  // uint8_t _setBrightnessValue[5] = {0, 0, 0, 80, 80};      // The target percent value of the dimmer
  // int8_t _step[5] = {0, 0, 0, 0, 0};           // Every _step milliseconds the corresponding dimmer value is incremented or decremented

  // float _setColorTempValue[2] = {0.0f, 0.0f};     //Color-temp as difference between CW and WW //only two are used but for better readability all 5 are saved

  // // struct CHSV _curHsv; // The current HSV values

  // uint16_t _loopCount = 0;

  // EFFECTMODE _effectMode = emFADE;
  // EFFECTMODE _effectModeLast = emFADE;
  // EFFECTSTATE _effectState = esDONE;

  // Variables will change: For state "warning" only
  uint8_t warning_activeted = false;
  uint8_t speedBeforeWarning = 2;
  uint8_t ledState = LOW;             // ledState used to set the LED
  uint8_t maxBlinkCount = 3;             // after 3 blinks warning is done
  uint32_t previousMillis = 0;        // will store last time LED was updated
  uint8_t countHigh = 0;
  uint8_t countLow = 0;
  // constants won't change:
  const uint32_t interval = 500;           // interval at which to blink (milliseconds) in state "warning"

  // unsigned long _lastLoop = 0;
  // unsigned long _transitionTime = cStartTransitionTime;
  // unsigned long _waitTime = cStartTransitionTime / cFadeSteps;

  // void printCaption();

  // Helper functions for conversion between different normalizations
  // Standard = Byte to Percent and vice versa

  int toPercent(const int value, const int factor = 100);
  int toByte(const int value, const int factor = 100);
  int tryStrToInt(const String &value, const int maxvalue = 100);
  float tryStrToFloat(const String &value, const float maxvalue);


  void fadeToHSVConvert(int h, int s, int v);

  void fadeToHSV(CHSV hsvIn);
  // void fadeToRGB();

  bool parseHSV(const String &value);
  bool parseRGB(const String &value);

  // Methods for telling the MQTT-Broker the sucessfull change of the variables
  void setRGBState();
  void setCCTState();

  // void loopCycle();
  // void loopFade();
  void blinkCycle();

protected:
  virtual bool handleInput(const HomieRange &range, const String &property, const String &value);
  virtual void setup() override;
  virtual void loop() override;

public:
  H801Node(const char *id, const char *name);

  void beforeSetup();
  void afterSetup();
  void turnLightOn();
  void returnStatusToOpenhab();
  // void blackout();
};