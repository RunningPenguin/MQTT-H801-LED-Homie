/*
 * h801node.hpp
 * Homie node for a h801 five channel LED controller
 * usually RGBWW
 *
 * Version: 1.0
 * Author: Lübbe Onken (http://github.com/luebbe)
 */

#include <H801Node.hpp>

// uint16_t _light_mireds = round((LIGHT_COLDWHITE_MIRED+LIGHT_WARMWHITE_MIRED)/2);



H801Node::H801Node(const char *id, const char *name)
    : HomieNode(id, name, "RGBWW Controller")
{
}
int H801Node::toByte(const int value, const int factor)
{
  return value * 255 / factor;
}

int H801Node::toPercent(const int value, const int factor)
{
  return value * factor / 255;
}

int H801Node::tryStrToInt(const String &value, const int maxvalue)
{
  return constrain(value.toInt(), 0, maxvalue);
}

float H801Node::tryStrToFloat(const String &value, const float maxvalue)
{
  return constrain(value.toFloat(), 0, maxvalue);
}

void H801Node::fadeToHSVConvert(int h, int s, int v)
{
  // The FastLED hsv<->rgb converter works with values from 0..255
  // Input values from OpenHAB
  // H = 0°..360°
  // S = 0%..100%
  // V = 0%..100%
  // _endValue[s] are percent values again

  // Convert to byte value range
  _curHsv.hue = toByte(h, 360);
  _curHsv.sat = toByte(s);
  _curHsv.val = toByte(v);

  fadeToHSV(_curHsv);
}

void H801Node::fadeToHSV(CHSV hsvIn)
{
  struct CRGB rgbOut;

  // do the math
  hsv2rgb_rainbow(hsvIn, rgbOut);

  //rgb - RGB value in loxone format (BBBGGGRRR, each section would be 0-100). Example "100000000" would be 100% blue, "50" would be 50% red. (loxone format: R + G * 1000 + B * 1000000)
  //100100100 ... all three channels at 100%
  //50050050 ... all three channels at 50% 
  // convert back to percent values
  _curValue[COLORINDEX::RED] = toPercent(rgbOut.red);
  _curValue[COLORINDEX::GREEN] = toPercent(rgbOut.green);
  _curValue[COLORINDEX::BLUE] = toPercent(rgbOut.blue);
  rgb = _curValue[COLORINDEX::RED] + _curValue[COLORINDEX::GREEN] * 1000 + _curValue[COLORINDEX::BLUE] * 1000000;
  dimmer.dimRGB(COLORINDEX::RED, rgb, speed, /*message["curve"]*/0, /*message["onOffSpeed"]*/0);
  setRGBState();
}

bool H801Node::parseHSV(const String &value)
{
  // Expects H,S,V as comma separated values
  int h, s, v;

  if (sscanf(value.c_str(), "%d,%d,%d", &h, &s, &v) == 3)
  {
    fadeToHSVConvert(h, s, v);
    return true;
  }
  return false;
}

bool H801Node::parseRGB(const String &value)
{
  // Expects R,G,B as comma separated values
  int r, g, b;
  if (sscanf(value.c_str(), "%d,%d,%d", &r, &g, &b) == 3)
  {

    //rgb - RGB value in loxone format (BBBGGGRRR, each section would be 0-100). Example "100000000" would be 100% blue, "50" would be 50% red. (loxone format: R + G * 1000 + B * 1000000)
    //100100100 ... all three channels at 100%
    //50050050 ... all three channels at 50% 

    _curValue[COLORINDEX::RED] = toPercent(r);
    _curValue[COLORINDEX::GREEN] = toPercent(g);
    _curValue[COLORINDEX::BLUE] = toPercent(b);
    rgb = _curValue[COLORINDEX::RED] + _curValue[COLORINDEX::GREEN] * 1000 + _curValue[COLORINDEX::BLUE] * 1000000;
    dimmer.dimRGB(COLORINDEX::RED, rgb, speed, /*message["curve"]*/0, /*message["onOffSpeed"]*/0);
    setRGBState();
    return true;
  }
  return false;
}

bool H801Node::handleInput(const HomieRange &range, const String &property, const String &value)
{
  if (property == cPropOn)
  {
    if (value == "true")
    {
      cctBrightness=100;
  
      // dim a tunable white led, starting with warm white on channel, followed by cold white on channel + 1
      _curWhiteValues = dimmer.dimCCT(COLORINDEX::WHITEWW1, cctBrightness, Kelvin, speed, /*message["curve"]*/0, /*message["onOffSpeed"]*/0);
      setProperty(cPropCctBrightness).send(String(cctBrightness)); //give Feedback to Openhab

      setCCTState();
  
      // //return percentWW+percentCW*1000;
      // _curValue[COLORINDEX::WHITEWW1] = (uint8_t) (_curWhiteValues % 1000);
      // _curWhiteValues  = _curWhiteValues / 1000;
      // _curValue[COLORINDEX::WHITECW1] = (uint8_t) (_curWhiteValues % 1000);
      // setProperty(cPropWhite1).send(String(_curValue[COLORINDEX::WHITEWW1])); //give Feedback to Openhab
      // setProperty(cPropWhite2).send(String(_curValue[COLORINDEX::WHITECW1])); //give Feedback to Openhab
      // Not really settable, always reports true, because the controller is connected to a physical switch
      setProperty(cPropOn).send("true");
    }
    else if (value == "false")
    {
      cctBrightness=0;
  
      // dim a tunable white led, starting with warm white on channel, followed by cold white on channel + 1
      _curWhiteValues = dimmer.dimCCT(COLORINDEX::WHITEWW1, cctBrightness, Kelvin, speed, /*message["curve"]*/0, /*message["onOffSpeed"]*/0);
      setProperty(cPropCctBrightness).send(String(cctBrightness)); //give Feedback to Openhab

      setCCTState();

      uint8_t r = 0;
      uint8_t g = 0;
      uint8_t b = 0;
      rgb = r + g * 1000 + b * 1000000;
      //  setProperty(cPropRGB).send(rgbState);
       setProperty(cPropRed).send(String(r)); //give Feedback to Openhab
       setProperty(cPropGreen).send(String(g)); //give Feedback to Openhab
       setProperty(cPropBlue).send(String(b)); //give Feedback to Openhab
       // dim a RGB LED only use the first channel, it writes to all three rgb channels form its own
       dimmer.dimRGB(COLORINDEX::RED, rgb, speed, /*message["curve"]*/0, /*message["onOffSpeed"]*/0);
       setProperty(cPropOn).send("false");
    }



  }

  else if (property == cPropEffectMode)
  {
    if (value == "none")
    {
      oldSpeed = speed;
      speed = 255;
      setProperty(cPropEffectMode).send("none"); //give Feedback to Openhab
    }
    else if (value == "warning")
    {
      warning_activeted = true;
      setProperty(cPropEffectMode).send("warning"); //give Feedback to Openhab
    }
    else if (value == "fade-fast") //speed between 101...199
    {
      fadingSpeedHigh = true;
      if(oldSpeed != speed)
      {
        speed = oldSpeed;
      }
      if(speed <= 100)
      {
        speed = constrain(speed + 100, 101, 199);
      }
      if(speed >= 200)
      {
        speed = constrain(speed - 100, 101, 199);
      }
      setProperty(cPropEffectMode).send("fade-fast"); //give Feedback to Openhab
      // map(value, fromLow, fromHigh, toLow, toHigh)
      setProperty(cPropSpeed).send(String(map(speed,101,199,100,0))); //give Feedback to Openhab // values between 0...100
    }
    else if (value == "fade-slow") //speed between 1...99
    {
      fadingSpeedHigh = false;
      if(oldSpeed != speed)
      {
        speed = oldSpeed;
      }
      if(speed >= 200)
      {
        speed = constrain(speed - 200, 1, 99);
      }
      if(speed >= 100)
      {
        speed = constrain(speed - 100, 1, 99);
      }
      setProperty(cPropEffectMode).send("fade-slow"); //give Feedback to Openhab
      // map(value, fromLow, fromHigh, toLow, toHigh)
      setProperty(cPropSpeed).send(String(map(speed,1,99,100,0))); //give Feedback to Openhab // values between 0...100
    }
  }
  else if (property == cPropSpeed)
  {
    if(fadingSpeedHigh==false)
    {
      //speed - Optional. A factor for fading speed. Integer 0-255 (255 no fading, 1 fast, 99 slow, +100 4-times faster, +200 8-times faster)
      speed = 99 - tryStrToInt(value, 98); //A 0 on the Homie-MQTT calculates to 99 => slow, a 100 on Homie-MQTT calculates to 1 => fast
      // map(value, fromLow, fromHigh, toLow, toHigh)
      setProperty(cPropSpeed).send(String(map(speed,1,99,100,0))); //give Feedback to Openhab // values between 0...100
    }
    if(fadingSpeedHigh==true)
    {
      //speed - Optional. A factor for fading speed. Integer 0-255 (255 no fading, 1 fast, 99 slow, +100 4-times faster, +200 8-times faster)
      speed = 199 - tryStrToInt(value, 98); //A 0 on the Homie-MQTT calculates to 99 => slow, a 100 on Homie-MQTT calculates to 1 => fast
      // map(value, fromLow, fromHigh, toLow, toHigh)
      setProperty(cPropSpeed).send(String(map(speed,101,199,100,0))); //give Feedback to Openhab // values between 0...100
    }
  }
  else if (property == cPropHSV)
  {
// homie/buero-licht/LichtBuero/hsv/set 213,100,100
// homie/buero-licht/LichtBuero/hsv/set 0,100,100
// homie/buero-licht/LichtBuero/hsv/set 0,100,59
// homie/buero-licht/LichtBuero/hsv/set 0,55,59
    parseHSV(value);

    // dim a RGB LED only use the first channel, it writes to all three rgb channels form its own
    dimmer.dimRGB(COLORINDEX::RED, rgb, speed, /*message["curve"]*/0, /*message["onOffSpeed"]*/0);
    setProperty(cPropHSV).send(String(value)); //give Feedback to Openhab
  }
  else if (property == cPropRGB) //use hsv instead, did not work correctly
  {
    // parseRGB(value);

    // // dim a RGB LED only use the first channel, it writes to all three rgb channels form its own
    // dimmer.dimRGB(COLORINDEX::RED, rgb, speed, /*message["curve"]*/0, /*message["onOffSpeed"]*/0);
    // setRGBState();
  }
  else if (property == cPropRed)
  {
// Channel in  dmx.cpp
    // pwm_set_duty(dmxV[2], 0);  //BLUE
    // pwm_set_duty(dmxV[1], 1);  //GREEN
    // pwm_set_duty(dmxV[0], 2);  //RED
    // pwm_set_duty(dmxV[3], 3);  
    // pwm_set_duty(dmxV[4], 4); 
    //bri - brightness in percent
    //curve - Optional. Dimming curve. Integer (0 - linear, 1-3 logarithmic curve)
    //onOffSpeed, (onOffSpeed == 0 ? speed : onOffSpeed)
    _curValue[COLORINDEX::RED] = tryStrToInt(value);
    dimmer.dimChannel(COLORINDEX::RED, _curValue[COLORINDEX::RED], speed, /*message["curve"]*/0, /*message["onOffSpeed"]*/0);
    setRGBState();
  }
  else if (property == cPropGreen)
  {
// Channel in  dmx.cpp
    // pwm_set_duty(dmxV[2], 0);  //BLUE
    // pwm_set_duty(dmxV[1], 1);  //GREEN
    // pwm_set_duty(dmxV[0], 2);  //RED
    // pwm_set_duty(dmxV[3], 3);  
    // pwm_set_duty(dmxV[4], 4); 
    //bri - brightness in percent
    //curve - Optional. Dimming curve. Integer (0 - linear, 1-3 logarithmic curve)
    //onOffSpeed, (onOffSpeed == 0 ? speed : onOffSpeed)
    _curValue[COLORINDEX::GREEN] = tryStrToInt(value);
    dimmer.dimChannel(COLORINDEX::GREEN, _curValue[COLORINDEX::GREEN], speed, /*message["curve"]*/0, /*message["onOffSpeed"]*/0);
    setRGBState();
  }
  else if (property == cPropBlue)
  {
// Channel in  dmx.cpp
    // pwm_set_duty(dmxV[2], 0);  //BLUE
    // pwm_set_duty(dmxV[1], 1);  //GREEN
    // pwm_set_duty(dmxV[0], 2);  //RED
    // pwm_set_duty(dmxV[3], 3);  
    // pwm_set_duty(dmxV[4], 4); 
    //bri - brightness in percent
    //curve - Optional. Dimming curve. Integer (0 - linear, 1-3 logarithmic curve)
    //onOffSpeed, (onOffSpeed == 0 ? speed : onOffSpeed)
    _curValue[COLORINDEX::BLUE] = tryStrToInt(value);
    dimmer.dimChannel(COLORINDEX::BLUE, _curValue[COLORINDEX::BLUE], speed, /*message["curve"]*/0, /*message["onOffSpeed"]*/0);
    setRGBState();
  }
  else if (property == cPropWhite1)
  {
// Channel in  dmx.cpp
    // pwm_set_duty(dmxV[2], 0);  //BLUE
    // pwm_set_duty(dmxV[1], 1);  //GREEN
    // pwm_set_duty(dmxV[0], 2);  //RED
    // pwm_set_duty(dmxV[3], 3);  
    // pwm_set_duty(dmxV[4], 4); 
    //bri - brightness in percent
    //curve - Optional. Dimming curve. Integer (0 - linear, 1-3 logarithmic curve)
    //onOffSpeed, (onOffSpeed == 0 ? speed : onOffSpeed)
    _curValue[COLORINDEX::WHITEWW1] =tryStrToInt(value);
    dimmer.dimChannel(COLORINDEX::WHITEWW1, _curValue[COLORINDEX::WHITEWW1], speed, /*message["curve"]*/0, /*message["onOffSpeed"]*/0);
    setProperty(cPropWhite1).send(String(_curValue[COLORINDEX::WHITEWW1])); //give Feedback to Openhab
  }
  else if (property == cPropWhite2)
  {
// Channel in  dmx.cpp
    // pwm_set_duty(dmxV[2], 0);  //BLUE
    // pwm_set_duty(dmxV[1], 1);  //GREEN
    // pwm_set_duty(dmxV[0], 2);  //RED
    // pwm_set_duty(dmxV[3], 3);  //WW
    // pwm_set_duty(dmxV[4], 4);  //CW
    //bri - brightness in percent
    //curve - Optional. Dimming curve. Integer (0 - linear, 1-3 logarithmic curve)
    //onOffSpeed, (onOffSpeed == 0 ? speed : onOffSpeed)
    _curValue[COLORINDEX::WHITECW1] = tryStrToInt(value);
    dimmer.dimChannel(COLORINDEX::WHITECW1, _curValue[COLORINDEX::WHITECW1], speed, /*message["curve"]*/0, /*message["onOffSpeed"]*/0);
    setProperty(cPropWhite2).send(String(_curValue[COLORINDEX::WHITECW1])); //give Feedback to Openhab
  }
  else if (property == cPropColorTemp)
  {
// MQTT Message from OpenHab homie/buero-licht/LichtBuero/colorTemp/set 0.54000000 = 54%
// homie/buero-licht/LichtBuero/colorTemp/set 1 = 100%
// homie/buero-licht/LichtBuero/colorTemp/set 0 = 0%
// homie/buero-licht/LichtBuero/colorTemp/set 0.01000000 = 1%
// homie/buero-licht/LichtBuero/colorTemp/set 0.99000000 = 99%

    // Kelvin = (uint16_t) ((tryStrToFloat(value, 99999.9) * (float) 4500.0) + (float) 2000.0);

    Kelvin = (uint16_t) constrain(value.toInt(), 2000, 6500);

    //colortemp - Color temperature in Kelvin. Only used in mode "cct".
    // dim a tunable white led, starting with warm white on channel, followed by cold white on channel + 1
    _curWhiteValues = dimmer.dimCCT(COLORINDEX::WHITEWW1, cctBrightness, Kelvin, speed, /*message["curve"]*/0, /*message["onOffSpeed"]*/0);
    setProperty(cPropColorTemp).send(String(Kelvin,0)); //give Feedback to Openhab

    setCCTState();
  }
  else if (property == cPropCctBrightness)
  {
// MQTT Message from OpenHab homie/buero-licht/LichtBuero/brightness/set 54 = 54%
// homie/buero-licht/LichtBuero/brightness/set 100 = 100%
// homie/buero-licht/LichtBuero/brightness/set 0 = 0%
// homie/buero-licht/LichtBuero/brightness/set 1 = 1%
// homie/buero-licht/LichtBuero/brightness/set 99 = 99%

    cctBrightness=tryStrToInt(value);

    // dim a tunable white led, starting with warm white on channel, followed by cold white on channel + 1
    _curWhiteValues = dimmer.dimCCT(COLORINDEX::WHITEWW1, cctBrightness, Kelvin, speed, /*message["curve"]*/1, /*message["onOffSpeed"]*/0);
    setProperty(cPropCctBrightness).send(String(cctBrightness)); //give Feedback to Openhab

    setCCTState();
  }

  return true;
}

void H801Node::setRGBState()
{
   char rgbState[20];
   sprintf(rgbState, "%d,%d,%d", _curValue[COLORINDEX::RED], _curValue[COLORINDEX::GREEN], _curValue[COLORINDEX::BLUE]);
   setProperty(cPropRGB).send(rgbState);
   setProperty(cPropRed).send(String(_curValue[COLORINDEX::RED])); //give Feedback to Openhab
   setProperty(cPropGreen).send(String(_curValue[COLORINDEX::GREEN])); //give Feedback to Openhab
   setProperty(cPropBlue).send(String(_curValue[COLORINDEX::BLUE])); //give Feedback to Openhab
      // _curValue[COLORINDEX::WHITEWW1];
      // _curValue[COLORINDEX::WHITECW1];
}

void H801Node::setCCTState()
{
    //return percentWW+percentCW*1000;
    _curValue[COLORINDEX::WHITEWW1] = (uint8_t) ((_curWhiteValues % 1000));
    _curWhiteValues  = _curWhiteValues / 1000;
    _curValue[COLORINDEX::WHITECW1] = (uint8_t) ((_curWhiteValues % 1000));

    if (_curValue[COLORINDEX::WHITEWW1] > 100)
    {
      _curValue[COLORINDEX::WHITEWW1] = toPercent(_curValue[COLORINDEX::WHITEWW1]);
    }
    if (_curValue[COLORINDEX::WHITECW1] > 100)
    {
      _curValue[COLORINDEX::WHITECW1] = toPercent(_curValue[COLORINDEX::WHITECW1]);
    }


    setProperty(cPropWhite1).send(String(_curValue[COLORINDEX::WHITEWW1])); //give Feedback to Openhab
    setProperty(cPropWhite2).send(String(_curValue[COLORINDEX::WHITECW1])); //give Feedback to Openhab
}

void H801Node::loop()
{
  if (warning_activeted == true)
  {
    blinkCycle();
  }
}

void H801Node::blinkCycle()
{

    uint32_t currentMillis = millis();
    // one step each _transitionTime in milliseconds
    if ((currentMillis - previousMillis >= interval) || (previousMillis == 0))
    {
      previousMillis = currentMillis;
    // if the LED is off turn it on and vice-versa:
      if (ledState == LOW)
      {
        dimmer.dimChannel(COLORINDEX::RED, constrain((_curValue[COLORINDEX::RED] - 20), 0, 100), 255, /*message["curve"]*/0, /*message["onOffSpeed"]*/255);
        dimmer.dimChannel(COLORINDEX::GREEN, constrain((_curValue[COLORINDEX::GREEN] - 20), 0, 100), 255, /*message["curve"]*/0, /*message["onOffSpeed"]*/255);
        dimmer.dimChannel(COLORINDEX::BLUE, constrain((_curValue[COLORINDEX::BLUE] - 20), 0, 100), 255, /*message["curve"]*/0, /*message["onOffSpeed"]*/255);
        dimmer.dimCCT(COLORINDEX::WHITEWW1, constrain((cctBrightness - 20), 0, 100), Kelvin, 255, /*message["curve"]*/0, /*message["onOffSpeed"]*/255);
        ledState = HIGH;
        countHigh++;
      }
      else
      {
        dimmer.dimChannel(COLORINDEX::RED, constrain((_curValue[COLORINDEX::RED] + 20), 0, 100), 255, /*message["curve"]*/0, /*message["onOffSpeed"]*/255);
        dimmer.dimChannel(COLORINDEX::GREEN, constrain((_curValue[COLORINDEX::GREEN] + 20), 0, 100), 255, /*message["curve"]*/0, /*message["onOffSpeed"]*/255);
        dimmer.dimChannel(COLORINDEX::BLUE, constrain((_curValue[COLORINDEX::BLUE] + 20), 0, 100), 255, /*message["curve"]*/0, /*message["onOffSpeed"]*/255);
        dimmer.dimCCT(COLORINDEX::WHITEWW1, constrain((cctBrightness + 20), 0, 100), Kelvin, 255, /*message["curve"]*/0, /*message["onOffSpeed"]*/255);
        ledState = LOW;
        countLow++;
      }
    }

    if(countHigh >=maxBlinkCount && countLow >=maxBlinkCount)
    {
      countHigh = 0;
      countLow = 0;

      dimmer.dimChannel(COLORINDEX::RED, constrain((_curValue[COLORINDEX::RED]), 0, 100), 255, /*message["curve"]*/0, /*message["onOffSpeed"]*/255);
      dimmer.dimChannel(COLORINDEX::GREEN, constrain((_curValue[COLORINDEX::GREEN]), 0, 100), 255, /*message["curve"]*/0, /*message["onOffSpeed"]*/255);
      dimmer.dimChannel(COLORINDEX::BLUE, constrain((_curValue[COLORINDEX::BLUE]), 0, 100), 255, /*message["curve"]*/0, /*message["onOffSpeed"]*/255);
      dimmer.dimCCT(COLORINDEX::WHITEWW1, constrain((cctBrightness), 0, 100), Kelvin, 255, /*message["curve"]*/0, /*message["onOffSpeed"]*/255);
      warning_activeted = false;

      if(speed>200 && speed <255)
      {
        setProperty(cPropEffectMode).send("fade-fast"); //give Feedback to Openhab
      }
      if(speed<200)
      {
        setProperty(cPropEffectMode).send("fade-slow"); //give Feedback to Openhab
      }
      if(speed == 255)
      {
        setProperty(cPropEffectMode).send("none"); //give Feedback to Openhab
      }

    }

}


void H801Node::beforeSetup()
{
  advertise(cPropOn).setName("on/off Light").setDatatype("boolean").settable();         // on/off = true/false
  advertise(cPropEffectMode).setDatatype("enum").setFormat("none,warning,fade-fast,fade-slow").settable(); // Effect mode (none|fade|cycle)
  advertise(cPropSpeed).setDatatype("integer").setFormat("0:100").setUnit("%").settable();         // Transition speed for colors and effects
  advertise(cPropHSV).setDatatype("color").setFormat("hsv").settable();        // Expects H,S,V as comma separated values (Hue=0°..360°, Sat=0%..100%, Val=0%..100%)
  // advertise(cPropRGB).setDatatype("color").setFormat("rgb").settable();        // Expects R,G,B as comma separated values (R,G,B=0%..100%) //30.08.2020 new expects R,G,B=0...255 compatible with new Homie3.0.0 did not work correctly use hsv instead
  advertise(cPropRed).setDatatype("integer").setFormat("0:100").setUnit("%").settable();     // RGB values from 0% to 100%
  advertise(cPropGreen).setDatatype("integer").setFormat("0:100").setUnit("%").settable();   //
  advertise(cPropBlue).setDatatype("integer").setFormat("0:100").setUnit("%").settable();    //
  // advertise("hue").settable();        // hue from 0° to 360°
  // advertise("saturation").settable(); // from 0% to 100%
  // advertise("value").settable();      // from 0% to 100%
  advertise(cPropWhite1).setDatatype("integer").setFormat("0:100").setUnit("%").settable(); // White channels from 0% to 100%
  advertise(cPropWhite2).setDatatype("integer").setFormat("0:100").setUnit("%").settable(); //
  advertise(cPropColorTemp).setDatatype("integer").setFormat("2700:6500").setUnit("%").settable(); //
  advertise(cPropCctBrightness).setDatatype("integer").setFormat("0:100").setUnit("%").settable(); //
}


void H801Node::setup()
{

}

void H801Node::afterSetup()
{

}

void H801Node::turnLightOn()
{
  _curValue[COLORINDEX::RED] = 100;
  _curValue[COLORINDEX::GREEN] = 10;
  _curValue[COLORINDEX::BLUE] = 20;

  rgb = _curValue[COLORINDEX::RED] + _curValue[COLORINDEX::GREEN] * 1000 + _curValue[COLORINDEX::BLUE] * 1000000;

  dimmer.dimRGB(COLORINDEX::RED, rgb, 255, /*message["curve"]*/0, /*message["onOffSpeed"]*/255);

  _curWhiteValues = dimmer.dimCCT(COLORINDEX::WHITEWW1, cctBrightness, Kelvin, 255, /*message["curve"]*/0, /*message["onOffSpeed"]*/255);
}

void H801Node::returnStatusToOpenhab()
{
  delay(20);
  setRGBState();
  setProperty(cPropOn).send("true");
  setProperty(cPropEffectMode).send("fade-slow"); //give Feedback to Openhab
  setProperty(cPropSpeed).send(String(200-speed)); //give Feedback to Openhab
  setCCTState();
  setProperty(cPropColorTemp).send(String(Kelvin, 0)); //give Feedback to Openhab
  setProperty(cPropCctBrightness).send(String(cctBrightness)); //give Feedback to Openhab
  delay(400);
}