


# Homie MQTT H801 LED

Homie implementation for ESP based H801 LED-dimmer.
Supports one CCT channels and one RGB channel.
The API is exposed to the MQTT broker by following the Homie standardisation.

Right now there is support for:
* H801 - LED Dimmer

Firmware which exposes one CCT channels and one RGB channel to an MQTT Broker.

The Firmware uses the Homie 3.0.1 convention so auto detection in OpenHab is working (tested with Version 3.0 and 2.5.9).

[![works with MQTT Homie](https://homieiot.github.io/img/works-with-homie.svg "works with MQTT Homie")](https://homieiot.github.io/)

## Download

The Git repository contains the development version of these firmware. It is possible that there are some bugs.
## license
- https://creativecommons.org/licenses/by-nc-sa/3.0/ CC BY-NC-SA 3.0
- Use at your own risk
- everything is tested as good as I can but I couldn't give any guarantee
## Using with PlatformIO

[PlatformIO](http://platformio.org) is an open source ecosystem for IoT development with cross platform build system, library manager and full support for Espressif ESP8266 development. It works on the popular host OS: Mac OS X, Windows, Linux 32/64, Linux ARM (like Raspberry Pi, BeagleBone, CubieBoard).

1. Install [PlatformIO IDE](http://platformio.org/platformio-ide)
2. Create new project using "PlatformIO Home > New Project"
3. Open [Project Configuration File `platformio.ini`](http://docs.platformio.org/page/projectconf.html)


-----
Happy coding with PlatformIO!

## Features

* Automatic connection/reconnection to Wi-Fi/MQTT
* exposes one CCT channels to homie
* exposes one RGB channel to homie
* the CCT channel could be controleld indivually with color-temp and brightness
* the CCT channel could be controlled to blink to gather attention
* the RGB channel could be controlled to change color/hue/saturation and brightness
* all channels are not controlled by the device itself! They are controlled by the overlaying automation system (OpenHab, ...)
* it is possible to let each LED blink indiviually by a given frequency and an given amount how often the LED should blink
* not tested feature Homie OTA update - I couldn't get the firmware update server to work the firmware includes the magic bytes

## How to install
* information's comes later. I desoldered one pin inside of the Touch switch and added an pin header for the RX/TX/reset/GND and 5V lines to be able to flash with USB2TTL Uart device.

## Credits/thanks to the following projects
* https://github.com/homieiot/homie-esp8266
* https://github.com/steveh80/H801-mqtt

## The device configuration to the MQTT broker shows the following content

* will follow